var mastodon = "https://infosec.exchange/@ximnoise";
var gitlab = "https://gitlab.com/ximnoise";
var bootcamp = "https://careerfoundry.com";
var blog ="https://blog.maximilianrogge.com";
var i3autotiling = "https://gitlab.com/ximnoise/i3-auto-tiling";
var myFLixReact = "https://gitlab.com/ximnoise/myFlix-react-client";
var myFlixAngular = "https://gitlab.com/ximnoise/myFlix-angular-client";
var myFlixBackend = "https://gitlab.com/ximnoise/myFlix-backend";
var comgear = "https://gitlab.com/ximnoise/comgear";
var meet = "https://gitlab.com/ximnoise/meet";
var pokedex = "https://gitlab.com/ximnoise/pokedex";
var password = "fsociety";

help = [
  "<br>",
  '<span class="command">whois</span>',
  '   Who is Maximilian Rogge?',
  "<br>",
  '<span class="command">whoami</span>',
  '   Who are you?',
  "<br>",
  '<span class="command">contact</span>',
  '   Display social networks',
  "<br>",
  '<span class="command">blog</span>',
  '   View my blog',
  "<br>",
  '<span class="command">projects</span>',
  '   View coding projects',
  "<br>",
  '<span class="command">secret</span>',
  '   Find the password',
  "<br>",
  '<span class="command">history</span>',
  '   View command history',
  "<br>",
  '<span class="command">help</span>',
  '   You obviously already know what this does',
  "<br>",
  '<span class="command">clear</span>',
  '   Clear terminal',
  "<br>",
  '<span class="command">banner</span>',
  '   Display the header',
  "<br>",
];

whois = [
  "<br>",
  "Hey, there",
  "I'm Maximilian Rogge aka Ximnoise, a passionate self-taught junior-sysadmin and full-stack webdeveloper from germany with passion for cybersecurity.",
  "I am also an open-source enthusiast and I love how collaboration and knowledge sharing happened through open-source.",
  "Befor my career as a junior-sysadmin I worked as a machine operator for one of the biggest automotive supplier.",
  "While doing that, I started to dive into web development",
  'and began a Full-Stack-Webdeveloper Bootcamp at <a href="' + bootcamp + '" target="_blank">Careerfoundry.' + "</a>",
  "After 6 months I finished it and with the knowledge of an Full-Stack-Webeveloper",
  "and through private events I went deeper down the rebbit hole into cybersecurity and linux.",
  "<br>"
];

whoami = [
  "<br>",
  "The paradox of “Who am I?” is: we never know, but, we constantly find out.",
  "<br>"
];

contact = [
  "<br>",
  'mastodon       <a href="' + mastodon + '" target="_blank">mastodon/ximnoise' + "</a>",
  'gitlab         <a href="' + gitlab + '" target="_blank">gitlab/ximnoise' + "</a>",
  "<br>"
];

blog = [
  "<br>",
  'blog         <a href="' + blog + '" target="_blank">blog.maximilianrogge.com' + "</a>",
  "<br>"
];

projects = [
  "<br>",
  '<span class="command">i3-auto-tiling</span>',
  "   Automatic, optimal tiling for i3wm inspired by the deprecated i3-alternating-layouts and bspwm.",
  "   An appropriate split is set for each window based on its geometry.",
  '   gitlab         <a href="' + i3autotiling + '" target="_blank">i3-auto-tiling' + "</a>",
  "<br>",
  '<span class="command">myFlix Frontend</span>',
  "   Angular and React application that allows users to get information about movies, genres, and directors.",
  "   This application uses my existing server-side REST API and MongoDB database.",
  '   gitlab         <a href="' + myFlixAngular + '" target="_blank">myFlix Angular' + "</a>",
  '   gitlab         <a href="' + myFLixReact + '" target="_blank">myFlix React' + "</a>",
  "<br>",
  '<span class="command">myFlix Backend</span>',
  "   REST API that provide users with access to information about different movies, directors, and genres.",
  "   Users will be able to sign up, update their personal information, and create a list of their favorite movies.",
  '   gitlab         <a href="' + myFlixBackend + '" target="_blank">myFlix Backend' + "</a>",
  "<br>",
  '<span class="command">Comgear</span>',
  "   Mobile chat app built with React Native and Expo that stores data with Google Firebase.",
  "   Users can send messages and are able to share images and location.",
  '   gitlab         <a href="' + comgear + '" target="_blank">Comgear' + "</a>",
  "<br>",
  '<span class="command">Meet</span>',
  "   Meet App is a serverless, progressive web application (PWA) with React using a test-driven development (TDD) technique.",
  "   The application uses the Google Calendar API to fetch upcoming events. The serverless function is hosted by the cloud provider AWS.",
  '   gitlab         <a href="' + meet + '" target="_blank">Meet' + "</a>",
  "<br>",
  '<span class="command">Pokedex</span>',
  "   This small web app is a part of my Bootcamp at CareerFoundry.",
  "   It contains HTML, CSS/Bootstrap and JavaScript that loads data from a external Pokemon API and show the data end points in detail.",
  '   gitlab         <a href="' + pokedex + '" target="_blank">Pokedex' + "</a>",
  "<br>",
];

secret = [
  "<br>",
  '<span class="command">sudo</span>           Only use if you\'re admin',
  "<br>"
];

banner = [
  '<span class="index">Maximilian Rogge aka Ximnoise Not A Corporation. All rights reserved.</span>',
  "<br>",
  "  ███╗   ███╗ █████╗ ██╗  ██╗██╗███╗   ███╗██╗██╗     ██╗ █████╗ ███╗   ██╗    ██████╗  ██████╗  ██████╗  ██████╗ ███████╗",
  "  ████╗ ████║██╔══██╗╚██╗██╔╝██║████╗ ████║██║██║     ██║██╔══██╗████╗  ██║    ██╔══██╗██╔═══██╗██╔════╝ ██╔════╝ ██╔════╝",
  "  ██╔████╔██║███████║ ╚███╔╝ ██║██╔████╔██║██║██║     ██║███████║██╔██╗ ██║    ██████╔╝██║   ██║██║  ███╗██║  ███╗█████╗",
  "  ██║╚██╔╝██║██╔══██║ ██╔██╗ ██║██║╚██╔╝██║██║██║     ██║██╔══██║██║╚██╗██║    ██╔══██╗██║   ██║██║   ██║██║   ██║██╔══╝",
  "  ██║ ╚═╝ ██║██║  ██║██╔╝ ██╗██║██║ ╚═╝ ██║██║███████╗██║██║  ██║██║ ╚████║    ██║  ██║╚██████╔╝╚██████╔╝╚██████╔╝███████╗",
  "  ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝     ╚═╝╚═╝╚══════╝╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝    ╚═╝  ╚═╝ ╚═════╝  ╚═════╝  ╚═════╝ ╚══════╝ v1.6.0",
  "<br>",
  '<span class="color2">Welcome to my interactive portfolio web terminal.</span>',
  "<span class=\"color2\">For a list of available commands, type</span> <span class=\"command\">'help'</span><span class=\"color2\">.</span>",
];
