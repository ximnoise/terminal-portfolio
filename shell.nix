{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    live-server  
  ];

  shellHook = ''
    echo "########"
    echo "WEB DEV SHELL"
    echo "########"
  '';
}
